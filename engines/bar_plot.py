# -*- coding: utf-8 -*-
"""
Creating a bar plot to compare the differences between the current death toll/1m population for today,
and compare the difference between total recoveries and tests in other 5 different countries

@author: Phan Hieu Nghia 1834104
"""
import matplotlib.pyplot as plt
import pandas as pd


# This function gets deaths per million from the data frame using mask
def compare_deaths(data):
    try:
        # mask is a filter that tell the dataframe which entries to set to True
        # and you use it to get the data from the dataframe
        mask = data[0]['Country,Other'].isin(['USA', 'Brazil', 'India', 'Russia', 'UK'])
        countries = data[0][mask]
        deaths = countries['Deaths/1M pop'].values

        # Plotting the data into a bar plot
        deaths_per_mil = pd.DataFrame({'TODAYDEATHSPERMILLION': deaths},
                                      index=['USA', 'Brazil', 'India', 'Russia', 'UK'])
        deaths_per_mil.plot(kind='bar', title='TOTAL DEATHS PER MILLION of POPULATION')

        # Some modifications to the bar plots
        plt.xticks(rotation=360, horizontalalignment='center')
        plt.ylabel('Deaths per million population')

        plt.savefig('compare_deaths.png', dpi=400)  # save the bar plot
        plt.show()
        plt.close()
    except Exception as e:
        print('Error:', e)


# This function gets total recovered and total test using mask, separately
# Then using the two mask to make one bar plot, and set subplot=True to make it two bar plots
def compare_two_columns(data):
    try:
        mask = data[0]['Country,Other'].isin(['Japan', 'Hungary', 'Denmark', 'Guatemala', 'Latvia'])
        countries = data[0][mask]
        recoveries = countries['TotalRecovered'].values.astype('int64')

        mask2 = data[0]['Country,Other'].isin(['Japan', 'Hungary', 'Denmark', 'Guatemala', 'Latvia'])
        countries2 = data[0][mask2]
        testcases = countries2['TotalTests'].values.astype('int64')

        # Plotting the data into the bar plots
        index = ['Japan', 'Hungary', 'Denmark', 'Guatemala', 'Latvia']
        total = pd.DataFrame({'TOTALRECOVERS': recoveries,
                              'TOTALTESTS': testcases}, index=index)
        axes = total.plot(kind='bar', subplots=True)
        # After plotting the subplots, the code below modifies how it looks like
        axes[1].legend(loc=2)
        plt.xticks(rotation=360, horizontalalignment='center')
        plt.ylabel('Total tests in million')

        plt.savefig('compare_two_columns.png', dpi=400)  # save the bar plot
        plt.show()
        # plt.close()
    except Exception as e:
        print('Error:', e)
