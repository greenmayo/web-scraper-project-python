"""
Purpose of this script is to scrape data from "https://www.worldometers.info/coronavirus/"
and send it to a DataFrame object for further processing
As well as store the 'clean' data into 3 json files that correspond to the days.

A clean 'final' version of scrapper script

@author Parhomenco Kirill 1933830
"""
import pandas as pd
import requests
from bs4 import BeautifulSoup

#  header row contains all the headers from the table
__header_row = ['#', 'Country,Other', 'TotalCases', 'NewCases', 'TotalDeaths', 'NewDeaths', 'TotalRecovered',
                'NewRecovered', 'ActiveCases', 'Serious,Critical', 'TotCases/1M pop', 'Deaths/1M pop', 'TotalTests',
                'Tests/1M pop', 'Population', 'Continent', '1 Caseevery X ppl1', 'Deathevery X ppl1', 'Testevery X ppl']
#  rows that are about to be converted from string to integer for math purposes
__rows_to_change = ['TotalCases', 'TotalDeaths', 'TotalRecovered', 'ActiveCases', 'Serious,Critical', 'TotCases/1M pop',
                    'Deaths/1M pop', 'TotalTests', 'Tests/1M pop', 'Population', '1 Caseevery X ppl1',
                    'Deathevery X ppl1', 'Testevery X ppl']

#  link to the coronavirus data website
__URL = "https://www.worldometers.info/coronavirus/"


# create_dataframe is a helper method that works with list of rows for one particular day, passes through each row and
# extracts text from td. Rows from 9 to 230 are actual countries, the rest are world and continents, not necessary for
# our purposes and could be created artificially since we are keeping the 'Continent' column
# @return DataFrame for that particular scraped day
def create_dataframe(daily_data_tr):
    day_frame = pd.DataFrame(columns=__header_row)
    for entry in daily_data_tr[9:230]:
        row_entry = []  # need to clear that list every loop
        for i in entry('td'):
            row_entry.append(i.text)
        country = pd.Series(data=row_entry, index=day_frame.columns, name=row_entry[0])
        day_frame = day_frame.append(country)
    return day_frame


# helper method for converting columns of string to int. Numbers are in '123,456' format, so we are deleting comma
# some of the values have spaces to them at the end, getting rid of that too
# Netherlands and probably somewhere else has N/A value, which we just convert to nothing and to 0 for calculation
# purposes. Probably not the best choice but it is not my fault their data is faulty. Even Micronesia has numbers!
# returns modified and ready-to-math column
def convert_to_int(dataframe_column):  # returns modified column of ints
    try:
        dataframe_column = dataframe_column.str.replace(',', '')
        dataframe_column = dataframe_column.str.replace(' ', '')  # some rows have unnecessary whitespace
        dataframe_column = dataframe_column.str.replace('N/A', '')  # and some rows have that
        dataframe_column.replace('', 0, inplace=True)  # replace of a Series, can use inplace=True
        dataframe_column = dataframe_column.astype(int)  # to be able to perform math
    except ValueError as e:
        print("Something went wrong, trying float", e)
        print("Column Name: ", dataframe_column.name)
        dataframe_column = dataframe_column.astype(float).astype(int)
    #     The error that this exceptions catches was left there intentionally. Column that it does not convert is a
    #     ratio deaths/1Mpop. It does not convert it due to a dot in the string.
    #     We don't really need this 'artificial' data, but I thought it would be fun to have it here as an 'error'
    #  P.S.   For further calculation purposes the column was cast to float and back to integer, as what after the comma
    #     is not so relevant.
    finally:  # I've decided to put return in finally, wonder what bugs could it give at some point
        return dataframe_column


# function that calls other functions. Takes the page, loads into soup, parses it and extracts data for 3 days into 3
# tables DataFrames. Changes columns to integer, sorts it all again (because of China jumping around), stores to JSON
# and returns a tuple of 3 days DataFrames, ready to be worked on. More detailed information in coding journals
def scrape_data_soup():
    try:
        page = requests.get(__URL)
        if page.status_code != 200:
            raise ConnectionError('Not a 200 code')
        #  Unknown problem with china being on 221, 1 and 1 place
        soup = BeautifulSoup(page.content, 'html.parser')
        tables_today = soup.find(id='main_table_countries_today')  # this and next 2 are fetching tables from days
        tables_yesterday = soup.find(id='main_table_countries_yesterday')
        tables_yesterday2 = soup.find(id='main_table_countries_yesterday2')
        #  there probably is more elegant solution, however tunnel vision is a factor here
        #  creating dataframes using my helper method
        df_today = create_dataframe(tables_today('tr'))
        df_yesterday = create_dataframe(tables_yesterday('tr'))
        df_yesterday2 = create_dataframe(tables_yesterday2('tr'))

        #  changing primordial data columns to int for calculations and sorting the data based on total cases
        #  all because of China, I wonder why it does that.
        for i in __rows_to_change:
            df_today[i] = convert_to_int(df_today[i])
            df_yesterday[i] = convert_to_int(df_yesterday[i])
            df_yesterday2[i] = convert_to_int(df_yesterday2[i])
        # following 6 lines are sorting the dataframes and updating indexes, all because of China
        df_today.sort_values(by='TotalCases', ascending=False, inplace=True, ignore_index=True)
        df_yesterday.sort_values(by='TotalCases', ascending=False, inplace=True, ignore_index=True)
        df_yesterday2.sort_values(by='TotalCases', ascending=False, inplace=True, ignore_index=True)
        df_today['#'] = df_today.index + 1
        df_yesterday['#'] = df_yesterday.index + 1
        df_yesterday2['#'] = df_yesterday2.index + 1
        # storing each day in a separate Dataframe because that makes more sense, to separate data
        store_to_json([df_today, df_yesterday, df_yesterday2])
        return df_today, df_yesterday, df_yesterday2
    except ConnectionError as e:
        print("There was a problem with URL request")
        print(e)


# storing data to json in a separate method. I decided that storing data in 3 different files would be better since we
# dont need to load all of it to access just one day at some point. And if we want to monitor let's say for one month,
# i would store just 'yesterday' every day, commenting out 2 lines instead of changing code entirely
def store_to_json(scraped_data):
    #  data is ready to be stored in JSON
    scraped_data[0].to_json('country_neighbour_dist_file.json')
    scraped_data[1].to_json('country_neighbour_dist_file1.json')
    scraped_data[2].to_json('country_neighbour_dist_file2.json')
