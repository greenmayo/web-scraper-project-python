"""
File for data analysis
Idea behind this script is to compile a dataframe full of 'interesting' data.
It takes as input a tuple of 3 days data and input country from launcher.py. Then it uses defined columns and countries
to 'condense' the DataFrame for further processing. Using pandas builtins, we can easily find the difference between the
days and do something about it.

@author Parhomenco Kirill
"""

import pandas as pd

# add or delete columns to search for different data
__interesting_rows = ['Country,Other', 'TotalCases', 'TotalDeaths', 'TotalRecovered', 'ActiveCases', 'TotalTests']

# this variable represents columns of the newly created diff tables
__key_findings = ['CasesDiff', 'DeathsDiff', 'RecoveredDiff', 'ActiveDiff', 'TestsDiff']

# modify to compare the input country with something else
__keystone_countries = ['Canada', 'France', 'Germany', 'Russia']


# takes 3-day data and a country name to compare it to other 4 countries given above
# execution: takes a day, applies a mask to it that filters only the 5 countries, then deletes the rows that we dont use
# and transfers that to a separate DataFrame, indexed by Country,Other
# returns a tuple of 3 DF of sieved data for 5 countries, special rows.
def analyze_countries(three_day_data, country_name):
    __keystone_countries.insert(0, country_name)  # putting the new country first
    days_data_condensed = []
    for tdd in three_day_data:
        mask = tdd['Country,Other'].isin(__keystone_countries)
        five_countries_data = tdd[mask]
        fcd_condensed = five_countries_data[__interesting_rows].copy()
        fcd_condensed.set_index('Country,Other', inplace=True)
        days_data_condensed.append(fcd_condensed)
    percent_diff(find_diff_tables(days_data_condensed))
    return days_data_condensed


# creates 2 tables from 3 days with numbers of difference of the columns
# 0 is today, 1 is yesterday and 2 is yesterday2
def find_diff_tables(three_day_data):
    diff_table = pd.DataFrame(pd.DataFrame(three_day_data[0] - three_day_data[1]).values, index=three_day_data[0].index,
                              columns=__key_findings)
    diff_table2 = pd.DataFrame(pd.DataFrame(three_day_data[1] - three_day_data[2]).values,
                               index=three_day_data[0].index, columns=__key_findings)
    print('Today VS Yesterday')
    print(diff_table)
    print('Yesterday VS the day Before')
    print(diff_table2)
    return diff_table, diff_table2


# at this point I was not sure what kind of data I could create, so I made a table of %increase/decrease between
# today/yesterday and yesterday/yesterday2, because why not.
# this can be extrapolated on any kind of calculations
def percent_diff(diff_tables):
    inc_dec_table = diff_tables[0] * 100 / diff_tables[1]
    print("Here is how this 'night' is different from the previous one (in %):")
    print(inc_dec_table)
