# -*- coding: utf-8 -*-
"""
Establish a connection to the MySQL database and convert the existing dataframe to MySQL

@author: Phan Hieu Nghia 1834104
"""
# I kept this for future reference

# from sqlalchemy import create_engine
# import mysql.connector
# The reason why I used sqlalchemy because it the best library for the communication between Python code and database, allowing
# me for easy database injection. For sqlalchemy to work on your computer, you will need to install SQLAlchemy,mysqlclient and pymysql

# def df_to_sql(three_day_data):
#     engine = create_engine("mysql+mysqldb://alec:123@localhost:3306/testdb") 
#     #the syntax for the connection: dialect+driver://username:password@host:port/database
#     try: 
#         #the code will automatically take the data from dataframe and inject it into sql,
#         # automatically creating tables with defined columns
#         three_day_data[0].to_sql(name='covid_todays', con=engine, if_exists='replace', index=False,
#                                   chunksize=1000)
#         three_day_data[1].to_sql(name='covid_yesterday', con=engine, if_exists='replace', index=False,
#                                   chunksize=1000)
#         three_day_data[2].to_sql(name='covid_yesterday2', con=engine, if_exists='replace', index=False,
#                                   chunksize=1000)
#         print('Successfully written to database!')
#     except ValueError as ve:
#         print('Error:', ve)
#     except Exception as e:
#         print('Error2:', e)

import mysql.connector


def to_sql(three_day_data):
    mydb = mysql.connector.connect(
        host='localhost',
        user='alec',
        password='123',
        database="lab03"
    )

    # Drop the table if it exists
    try:
        mycursor = mydb.cursor(buffered=True)
        mycursor.execute('DROP TABLE IF EXISTS covid_today')
        mycursor.execute('DROP TABLE IF EXISTS covid_yesterday')
        mycursor.execute('DROP TABLE IF EXISTS covid_yesterday2')

        mydb.commit()
        print('Tables dropped')
    except mysql.connector.Error:
        print('No such tables exist')

    # Create the tables
    try:
        today = 'CREATE TABLE covid_today (Num BIGINT, Country TEXT, TotalCases INT, NewCases TEXT, TotalDeaths INT, NewDeath TEXT, TotalRecovered INT, NewRecovered TEXT, ActiveCases INT, Serious_critical INT, Tot_Cases_1M_pop INT, Deaths_1M_pop INT, TotalTests INT, Test_1M_pop INT, Population INT, Continent TEXT, Cases_every_X_ppl INT, Death_every_X_ppl INT, Test_every_X_ppl INT)'
        yesterday = 'CREATE TABLE covid_yesterday (Num BIGINT, Country TEXT, TotalCases INT, NewCases TEXT, TotalDeaths INT, NewDeath TEXT, TotalRecovered INT, NewRecovered TEXT, ActiveCases INT, Serious_critical INT, Tot_Cases_1M_pop INT, Deaths_1M_pop INT, TotalTests INT, Test_1M_pop INT, Population INT, Continent TEXT, Cases_every_X_ppl INT, Death_every_X_ppl INT, Test_every_X_ppl INT)'
        yesterday2 = 'CREATE TABLE covid_yesterday2 (Num BIGINT, Country TEXT, TotalCases INT, NewCases TEXT, TotalDeaths INT, NewDeath TEXT, TotalRecovered INT, NewRecovered TEXT, ActiveCases INT, Serious_critical INT, Tot_Cases_1M_pop INT, Deaths_1M_pop INT, TotalTests INT, Test_1M_pop INT, Population INT, Continent TEXT, Cases_every_X_ppl INT, Death_every_X_ppl INT, Test_every_X_ppl INT)'

        mycursor.execute(today)
        mycursor.execute(yesterday)
        mycursor.execute(yesterday2)

        mydb.commit()
        print('Tables created')
    except mysql.connector.Error:
        print('Tables not created')

    # Insert the data into the tables
    # First table
    try:
        today_sql = 'INSERT INTO covid_today(Num, Country, TotalCases, NewCases, TotalDeaths, NewDeath, TotalRecovered, NewRecovered, ActiveCases, Serious_critical, Tot_Cases_1M_pop, Deaths_1M_pop, TotalTests, Test_1M_pop, Population, Continent, Cases_every_X_ppl, Death_every_X_ppl, Test_every_X_ppl) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'
        for row in three_day_data[0].itertuples():
            mycursor.execute(today_sql, (
            row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13],
            row[14], row[15], row[16], row[17], row[18], row[19]))
        mydb.commit()
        print('Data was inserted')
    except mysql.connector.Error:
        print('Data was not inserted')

    # Second table
    try:
        yesterday_sql = 'INSERT INTO covid_yesterday(Num, Country, TotalCases, NewCases, TotalDeaths, NewDeath, TotalRecovered, NewRecovered, ActiveCases, Serious_critical, Tot_Cases_1M_pop, Deaths_1M_pop, TotalTests, Test_1M_pop, Population, Continent, Cases_every_X_ppl, Death_every_X_ppl, Test_every_X_ppl) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'
        for row in three_day_data[1].itertuples():
            mycursor.execute(yesterday_sql, (
            row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13],
            row[14], row[15], row[16], row[17], row[18], row[19]))
        mydb.commit()
        print('Data was inserted')
    except mysql.connector.Error:
        print('Data was not inserted')

    # Third table
    try:
        yesterday2_sql = 'INSERT INTO covid_yesterday2(Num, Country, TotalCases, NewCases, TotalDeaths, NewDeath, TotalRecovered, NewRecovered, ActiveCases, Serious_critical, Tot_Cases_1M_pop, Deaths_1M_pop, TotalTests, Test_1M_pop, Population, Continent, Cases_every_X_ppl, Death_every_X_ppl, Test_every_X_ppl) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s);'
        for row in three_day_data[2].itertuples():
            mycursor.execute(yesterday2_sql, (
            row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12], row[13],
            row[14], row[15], row[16], row[17], row[18], row[19]))
        mydb.commit()
        print('Data was inserted')
    except mysql.connector.Error:
        print('Data was not inserted')
