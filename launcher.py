"""
@author Parhomenco Kirill, Phan Hieu Nghia 1834104
Launcher script that invokes all the important functions, nothing else
"""

from engines import bar_plot
from engines import covid_scrapper
from engines import data_analysis
from engines import sql_con

three_day_data = covid_scrapper.scrape_data_soup()

sql_con.to_sql(three_day_data)

bar_plot.compare_deaths(three_day_data)
bar_plot.compare_two_columns(three_day_data)
country = input("Please choose a country: ")
# country = 'Moldova'
data_analysis.analyze_countries(three_day_data, country)
print('Thank you for using covid scrapper, stay happy and healthy!')
