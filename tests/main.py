"""
 @author Parhomenco Kirill 1933830
 Web scraping project

This file is like a scrap paper, not meant to be run, not being used by any other classes
or does not call any other files. It was created as a starting point for the functions and the sole purpose is
to test certain interactions between libraries in a script environment
"""

import pandas as pd
import requests
from bs4 import BeautifulSoup

URL = "https://www.worldometers.info/coronavirus/"
page = requests.get(URL)
soup = BeautifulSoup(page.content, 'html.parser')  # default parsing code
tables_today = soup.find(id='main_table_countries_today')  # this and next 2 are fetching tables from days
tables_yes = soup.find(id='main_table_countries_yesterday')
tables_yes2 = soup.find(id='main_table_countries_yesterday2')
nice_data = tables_today("tr")  # fetches all rows
# peru = nice_data[27].text.split('\n')  # to split the string into a list for extraction, \n to have a constant number of elements
#  P.S. splitting by \n wont work, last 3 column are not separated by breaks, so the 3 numbers stick to each other.
# table has 18 columns in total
# we need to create a header for all these elements
header_row = ['#', 'Country,Other', 'TotalCases', 'NewCases', 'TotalDeaths', 'NewDeaths', 'TotalRecovered',
              'NewRecovered', 'ActiveCases', 'Serious,Critical', 'TotCases/1M pop', 'Deaths/1M pop', 'TotalTests',
              'Tests/1M pop', 'Population', 'Continent', '1 Caseevery X ppl1', 'Deathevery X ppl1', 'Testevery X ppl']
dataframe = pd.DataFrame(columns=header_row)
# tables = []  # this will contain the list to input to DataFrame
# tabled = []  # testing purposes for row data entry
# for i in nice_data[9]('td'):
#     tabled.append(i.text)
# country = pd.Series(data=tabled, index=dataframe.columns, name=tabled[0])
# tabled = []
# dataframe = dataframe.append(country)
# for i in nice_data[10]('td'):
#     tabled.append(i.text)
# country2 = pd.Series(data=tabled, index=dataframe.columns, name=tabled[0])
# dataframe = dataframe.append(country2)

# one loop to rule them all and in DataFrame bind them
# for t in nice_data[9:229]:
#     country = []
#     tabled = []
#     for i in t('td'):
#         tabled.append(i.text)
#     country = pd.Series(data=tabled, index=dataframe.columns, name=tabled[0])
#     dataframe = dataframe.append(country)
