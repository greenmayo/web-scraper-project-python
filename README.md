# Python project: Covid_scraper

## Project status: Completed

**Authors: Kirill Parhomenco & Phan Hieu Nghia**
Description: This project is about web scraping, pandas' usage to store data, and matplotlib to make bar plots Usage(
modules): pandas, matplotlib.pyplot, sqlalchemy, BeautifulSoup, mysqlclient, pymysql

How to use:

1. First you will need to install the above modules for the program (if you haven't installed them)
2. The project is stored in package and the main program is `launcher.py`
3. Click Run
